﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	private Vector2 velocity;
	public float smothTimeY;
	public float smothTimeX;
	
	public GameObject Player;
	
	public bool bounds;
	
	public Vector3 minCameraPos;
	public Vector3 maxCameraPos;
	void Start () 
	{
	 Player = GameObject.FindGameObjectWithTag("Player");
	}
    void FixedUpdate()
	{
		float posX =Mathf.SmoothDamp(transform.position.x,Player.transform.position.x,ref velocity.x,smothTimeX);
		float posY =Mathf.SmoothDamp(transform.position.y,Player.transform.position.y,ref velocity.y,smothTimeY);
		
		transform.position = new Vector3(posX, posY, transform.position.z);
		if(bounds)
		{
			transform.position = new Vector3(Mathf.Clamp(transform.position.x,minCameraPos.x,maxCameraPos.x),
			Mathf.Clamp(transform.position.y,minCameraPos.y,maxCameraPos.y),
			Mathf.Clamp(transform.position.z,maxCameraPos.z,maxCameraPos.z));
		}
	}	

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public int curHealth;
	public int maxHealth = 20;

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.name == "player")
		{
			curHealth -= 20;
		}
	}
}
